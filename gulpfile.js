'use strict';

var SCSS_SRC = './assets/scss/**/*.scss';
var CSS_DEST = './assets/css/';

var JS_SRC = './assets/js/**/*.js';
var JS_DEST = './assets/js/';
var NOT_JS_MIN =  '!./assets/js/**/*.min.js';

var gulp = require('gulp');
var sass = require('gulp-sass');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var watch = require('gulp-watch');
var plumber = require('gulp-plumber');
var jshint = require('gulp-jshint');
var gutil = require('gulp-util');
var autoprefixer = require('gulp-autoprefixer');

//var changed = require('gulp-changed');
//var cache = require('gulp-cached');

var onError = function (err) {
  console.log('An error occurred:', gutil.colors.magenta(err.message));
  gutil.beep();
  this.emit('end');
};

// Sassコンパイルタスク
gulp.task('sass-compile', function(){
  return gulp.src(SCSS_SRC)
      .pipe(plumber({ errorHandler: onError }))
     .pipe(sass({
     outputStyle: 'expanded',
     sourceComments: true
     }))
     .pipe(autoprefixer({
            browsers: [
            'last 2 version',
            'iOS >= 8.1', 
            'Android >= 4.4'
            ],
            cascade: false
     }))
    .pipe(gulp.dest(CSS_DEST))
    .pipe(cssmin())
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest(CSS_DEST));
});

// jsの圧縮リネーム

gulp.task('js-minify', function () {
 return gulp.src([JS_SRC, NOT_JS_MIN]) // jQueryなどの、すでに.minなjsは除外する。
  .pipe(plumber({ errorHandler: onError }))
  .pipe(jshint())
    .pipe(jshint.reporter('default'))
  .pipe(uglify())
    .pipe(rename({
      extname: '.min.js'
    }))
    .pipe(gulp.dest(JS_DEST));
});

// scssファイル群の変更を監視するタスク
/*
gulp.task('scss:watch', function(){
  var watcher = gulp.watch(SCSS_SRC, ['sass-compile']);
  watcher.on('change', function(event) {
    console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
  });
});
*/

// gulpのデフォルトタスクとしてscss:watchタスクを指定
gulp.task('default', ['sass-compile','js-minify'],function(){

  watch([SCSS_SRC], function(event){
        gulp.start(['sass-compile']); // sassに変更があったら実行。cssを吐き出すので下のwatchが動く。
          console.log('File ' + event.path + ' was ' + event.type + ', sass-compile running tasks...');
    });
    
     watch([JS_SRC,NOT_JS_MIN ], function(event){
      gulp.start(['js-minify']); // jsに変更があったら実行。.minしたjsを吐き出すので下のwatchが動く。
     console.log('File ' + event.path + ' was ' + event.type + ', jsmin running tasks...');
   });
   
   
});