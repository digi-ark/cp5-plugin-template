<?php
/*
 * Plugin Name: CP5 Plugin Template
 * Version: 1.0
 * Plugin URI: https://castpro-cms.com/
 * Description: This is your starter template for your next WordPress plugin.
 * Author: CASTPRO
 * Author URI: https://castpro-cms.com/
 * Requires at least: 4.0
 * Tested up to: 4.0
 *
 * Text Domain: cp5-plugin-template
 * Domain Path: /languages/
 *
 * @package WordPress
 * @author Hugh Lashbrooke
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

// Load plugin class files
require_once( 'includes/class-cp5-plugin-template.php' );
require_once( 'includes/class-cp5-plugin-template-settings.php' );

// Load plugin libraries
require_once( 'includes/lib/class-cp5-plugin-template-admin-api.php' );
require_once( 'includes/lib/class-cp5-plugin-template-post-type.php' );
require_once( 'includes/lib/class-cp5-plugin-template-taxonomy.php' );

/**
 * Returns the main instance of CP5_Plugin_Template to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return object CP5_Plugin_Template
 */
function CP5_Plugin_Template () {
	$instance = CP5_Plugin_Template::instance( __FILE__, '1.0.0' );

	if ( is_null( $instance->settings ) ) {
		$instance->settings = CP5_Plugin_Template_Settings::instance( $instance );
	}

	return $instance;
}

CP5_Plugin_Template();