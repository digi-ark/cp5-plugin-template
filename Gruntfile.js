module.exports = function (grunt) {
    grunt.initConfig({

        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: 'assets/css',
                    src: ['*.css', '!*.min.css'],
                    dest: 'assets/css',
                    ext: '.min.css'
                }]
            }
        },
        
        minified: {
            files: {
                src: ['/assets/js/*.js'],
            },
        },

        compass: { // Task
            compile: { // Target
                options: {
                    //   config: 'config.rb',         // Target options
                    sassDir: 'assets/scss',
                    cssDir: 'assets/css',
                    imagesPath: 'assets/images',
                    javascriptsPath: 'assets/js',
                    outputStyle: 'expanded'
                        //  sassPath: 'scss',
                        //  cssDir: '',
                        //  environment: 'production'
                }
            },
            // dev: {                    // Another target
            //options: {
            // sassDir: 'scss',
            //sassPath: 'scss',
            //    cssDir: 'css'
            //}
            // }
        },
        watch: {
            //       options: {
            //        spawn: false
            //     },
            css: {
                files: ['assets/css/*.css', '!assets/css/*.min.css'],
                tasks: ['cssmin']
            },
            js: {
                files: 'assets/js/*.js', // 監視対象ファイル
                tasks: ['minified'] // 実行させるタスク
            },
            scss: {
                files: [
                    'assets/scss/*.scss',
                    'scss/**/*.scss',
                ],
                tasks: ['compass']
            }
        }
    });


    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-minified');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    //grunt.loadNpmTasks('grunt-contrib-uglify');

    //grunt.registerTask('default', ['uglify']);

    grunt.loadNpmTasks('grunt-contrib-concat');
};
